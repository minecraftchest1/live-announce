#!/bin/bash

## Config vars 
tmp=`mktemp`
config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/live-announce"
config="${config_dir}/config.json"

cache_dir="${XDG_CACHE_HOME:-$HOME/.cache}/live-announce"
cache="${cache_dir}/cache.txt"

## Ensure config file exists
mkdir -p "${config_dir}"
touch ${config}

## Ensure cache file exists
mkdir -p "${cache_dir}"
touch ${cache}

## Save video json to tmp file
yt-dlp --flat-playlist -I 1 --dump-single-json --skip-download \
	"https://www.youtube.com/${YOUTUBE_CHANNEL}/streams" > ${tmp}

## Extract video data to vars
id=`jq '.entries[0].id' --raw-output ${tmp}`
live=`jq '.entries[0].live_status' --raw-output ${tmp}`
title=`jq '.entries[0].title' --raw-output ${tmp}`
url=`jq '.entries[0].url' --raw-output ${tmp}`

## Retrieve previous video information from config
old_live=` grep 'status: ' ${cache} | sed 's/status: //'`
old_id=` grep 'id: ' ${cache} | sed 's/id: //'`

## Remove tmp file
rm ${tmp}

#############################

info()
{
	echo "Info: ${1}"
}

##############

## Echo various variables
info "${id}"
info "${url}"
#echo ${live}

##############

if [[ "${old_live}" == "${live}" ]]
then
	info "Live status has not changed."
else
	info "Live status has changed."

	printf 'Video Title: %s\nVideo Url: %s\n' "${title}" "${url}" | \
		apprise -vv -t 'Linus Tech Tips is Live' \
		"ntfy://${NTFY_SERVER}/${NTFY_TOPIC}?click=${url}" \
		"mastodons://${MASTODON_ACCESS_TOKEN}@${MASTODON_SERVER}"
fi


#info "Old live status was: ${old_live}"
#info "New status is: ${live}"

echo "status: ${live}" > ${cache}
echo "id: ${id}" >> ${cache}
