# Youtube Live Announce

## Announce When a Live Stream Has Started on a Configured Youtube Channel

## Enviroment Vars

### `YOUTUBE_CHANNEL`
### `NTFY_SERVER`
### `NTFY_TOPIC`
### `MASTODON_ACCESS_TOKEN`
### `MASTODON_SERVER`

## Dependencies
 - pipenv
 - jq
 - yt-dlp
 - apprise

## Installation

``` bash
git clone https://gitlab.com/minecraftchest1/live-announce.git          # Clone source code
cd live-announce                                                        # Change into source directory
pipenv install                                                          # Install dependencies (Requires pipenv) (Doesn't install jq)

##### Run service as current user #####
mkdir "${XDG_CONFIG_HOME-${HOME}/.config}/systemd/user/" -p             # Ensure systemd user unit directory exists
cp live-announce.* "${XDG_CONFIG_HOME-"${HOME}/.config"}/systemd/user/" # Install systemd units as user units
# Edit unit to match installation
systemd --user daemon-reload                                            # Load unit files
systemd --user enable --now live-announce.timer                         # Enable and start timer

##### Run service as system #####
cp live-announce.* /etc/systemd/system/                                 # Install systemd units as system units
systemd daemon-reload                                                   # Load unit files
systemd enable --now live-announce.timer                                # Enable and start timer

```

## Configuration

Configuration is provided through enviroment variables. Enviroment variables are in
the systemd service file (`live-announce.service`) through the `Environment` option.
In addition, you will also need to update working dir and the path to `pipenv`
